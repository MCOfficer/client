/*
 * Copyright (C) 2023 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package com.sheepit.client.exception;

/**
 * When a request to the server generate this exception.
 * The client must wait a bit before retrying.
 */
public class SheepItExceptionWithRequiredWait extends SheepItException {
	public SheepItExceptionWithRequiredWait() {
		super();
	}
	
	public SheepItExceptionWithRequiredWait(String message) {
		super(message);
	}
	
	public String getHumanText() {
		return "Please wait a bit";
	}
	
	/** how much time, it needs to wait
	 *
	 * @return duration in ms
	 */
	public int getWaitDuration() {
		return 0;
	}
}
