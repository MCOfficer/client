package com.sheepit.client.datamodel;

import lombok.Data;
import lombok.ToString;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(strict = false, name = "chunk") @Data @ToString public class Chunk {
	
	@Attribute private String md5;
	
	@Attribute private String id;
	
}
